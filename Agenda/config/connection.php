<?php
    /* Variáveis PDO */
	$host = 'localhost';
	$user = 'root';
	$pass = '';
	$db = 'agenda_php';

    /* Concatenação das variáveis para detalhes da classe PDO */
    $detalhes_pdo = "mysql:host=$host;dbname=$db";

    // Tenta conectar
    try {
    	// Cria a conexão PDO
    	$conexao_pdo = new PDO($detalhes_pdo, $user, $pass);
		// Ativar o modo erros
		$conexao_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } // try

    // Se algo der errado
    catch (PDOException $e) {
    	// Se der algo errado, mostra o erro PDO
    	print "Erro: " . $e->getMessage() . "<br/>";
    
    	// Mata o script
    	die();
    } // catch
?>
