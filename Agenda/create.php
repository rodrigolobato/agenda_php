<?php
    include_once("templates/header.php");
?>

<div class="container" >
    <div id="back-link-container">
        <a href="<?=$BASE_URL ?>index.php" id="back-link">Voltar</a>
    </div>
    <h1 id="main-title">Criar Contato</h1>
    <form id="create-form" action="<?=$BASE_URL ?>config/process.php" method="POST">
    <input type="hidden" name="type" value="create">
    <div class="form-group">
        <label for="name">Nome do contato:</label>
        <input type="text" class="form-control" name="name" id="name" placeholder="Digite o nome: " required>
    </div> 
    <div class="form-group">
        <label for="phone">Telefone de contato:</label>
        <input type="text" class="form-control" phone="phone" id="phone" placeholder="Digite o telefone: " required>
    </div> 
    <div class="form-group">
        <label for="observations">Observações:</label>
        <textarea type="text" class="form-control" observations="observations" id="observations" placeholder="Insira as observações: " rows="3" required></textarea>
    </div> 
    <button type="submit" class="btn btn-edit">Cadastrar</button>
    </form>
</div>

<?php
    include_once("templates/footer.php");
?>
