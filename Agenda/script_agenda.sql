CREATE DATABASE agenda_php;

USE agenda_php;

CREATE TABLE contacts(
	id INT NOT NULL AUTO_INCREMENT
	, name VARCHAR(200)
	, phone VARCHAR(20)
	, observations TEXT
	, PRIMARY KEY(id)
);


INSERT INTO `contacts` (`name`, `phone`, `observations`) 
VALUES ('Rodrigo Lobato', '(48)99661-7882', 'Programador Essential e Baixista.')
	   , ('Yuri', '(48)99999-9999', 'Programador Pleno Essential.')
	   , ('Leonardo Dosse', '(48)99888-5555', 'Programador Sênior Essential.')
	   , ('Bruno', '(48)99922-1111', 'QA Essential.');
